# Bitbucket Wiki Tidy Print

*Bitbucket Wiki Tidy Print* is a Chrome browser extension which removes superfluous toolbars and menus from Bitbucket's wiki pages during printing.

## Installation

1. Download sources from repository
2. Open Chrome's extension page (chrome://extensions)
3. Enable *developer mode*
4. Load extension from *Chrome* sub-folder.

## References

The extension icon is based on [David Vignoni's work][logo-source].

[logo-source]: https://www.iconfinder.com/iconsets/nuvola2
